import {
  Box,
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Divider,
  IconButton,
  Stack,
  Tab,
  Tabs,
  TextField,
} from '@mui/material'
import axios from 'axios'
import { useState } from 'react'
import { Typography } from '@mui/material'
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import 'dayjs/locale/th'
import dayjs, { Dayjs } from 'dayjs'
import CardHeader1 from '../../assets/amy.jpg'
import {
  DataGrid,
  GridRowsProp,
  GridColDef,
  GridToolbarContainer,
  GridToolbarExport,
} from '@mui/x-data-grid'

import TabContext from '@mui/lab/TabContext'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import { Pie } from 'react-chartjs-2'
import { saveAs } from 'file-saver'
import * as XLSX from 'xlsx' // for exporting to Excel
import { SystemUpdateAlt } from '@mui/icons-material'

const dataOp = {
  allCase: 0,
  allDebit: 0,
  ofcCase: 0,
  ofcDebit: 0,
  ucsInCase: 0,
  ucsInDebit: 0,
  ucsOutCase: 0,
  ucsOutDebit: 0,
  sssInCase: 0,
  sssInDebit: 0,
  sssOutCase: 0,
  sssOutDebit: 0,
  accInsCase: 0,
  accInsDebit: 0,
  lgoCase: 0,
  lgoDebit: 0,
  ucAeCase: 0,
  ucAEDebit: 0,
  ucHcCase: 0,
  ucHcDebit: 0,
  payCase: 0,
  payDebit: 0,
}

const apiUrl = import.meta.env.VITE_API_URL
// import { type } from './../../store/store';

type dataCaseOpProp = {
  no: number
  acc_code: string
  acc_name: string
  visit: string
  debt: string
}

export default function DebitIpSumPage() {
  const [dataCaseOp, setDataCaseOp] = useState<dataCaseOpProp[]>([])
  const [startDt, setStartDt] = useState<Dayjs | null>(dayjs(new Date()))
  const [endDt, setEndDt] = useState<Dayjs | null>(dayjs(new Date()))
  const [lastestOpd, setLastestOpd] = useState<string>('')

  const columns: GridColDef[] = [
    { field: 'no', headerName: 'no', width: 100 },
    { field: 'acc_code', headerName: 'รหัส', width: 200 },
    { field: 'acc_name', headerName: 'หมวดลูกหนี้', width: 400 },
    {
      field: 'visit',
      headerName: 'จำนวนครั้ง',
      width: 180,
      renderCell: (params) => {
        if (isNaN(params.value)) {
          return ''
        }

        const formattedNumber = Number(params.value).toLocaleString('en-US')

        return `${formattedNumber}`
      },
    },

    {
      field: 'debt',
      headerName: 'ค่าใช้จ่าย',
      width: 180,
      renderCell: (params) => {
        if (isNaN(params.value)) {
          return ''
        }

        const formattedNumber = Number(params.value).toLocaleString('en-US')

        return `${formattedNumber} ฿`
      },
    },
  ]

  const onSubmit = async () => {
    console.log({ startDt, endDt })

    let startDate = startDt?.format('YYYY-MM-DD')
    let endDate = endDt?.format('YYYY-MM-DD')

    // Op Case
    try {
      const responseOp = await axios.post(`${apiUrl}/debitipsum`, {
        startDate,
        endDate,
      })
      // setData(jsonData)

      console.log(responseOp.data.data)

      setDataCaseOp(responseOp.data.data)

      console.log(dataCaseOp)
    } catch (error) {
      console.log('ERROR', error)
    }
  }
  const CustomToolbar = () => {
    return (
      <GridToolbarContainer>
        <GridToolbarExport />
      </GridToolbarContainer>
    )
  }

  const [valueTab, setValueTab] = useState('1')

  const handleChangeTab = (event: React.SyntheticEvent, newValue: string) => {
    setValueTab(newValue)
  }

  const exportCaseToExcel = () => {
    const formattedData = dataCaseOp.map((row) => ({
      no: row.no,
      acc_code: row.acc_code,
      acc_name: row.acc_name,
      visit: parseInt(row.visit),
      debt: parseFloat(row.debt),
    }))

    const worksheet = XLSX.utils.json_to_sheet(formattedData)
    const workbook = XLSX.utils.book_new()
    XLSX.utils.book_append_sheet(workbook, worksheet, 'Data')
    const excelBuffer = XLSX.write(workbook, {
      bookType: 'xlsx',
      type: 'buffer',
    })

    saveAs(new Blob([excelBuffer]), 'data.xlsx')
  }

  return (
    <>
      <Stack direction={'row'} gap={2} height={560} sx={{ paddingLeft: 60 }}>
        <Card sx={{ width: '500px' }}>
          <CardMedia
            component={'img'}
            sx={{ height: 200, width: '100%' }}
            image={CardHeader1}
            title="green iguana"
          />

          <CardContent>
            <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="th">
              <Stack direction={'column'} gap={2}>
                <DatePicker
                  label="Start Date"
                  value={startDt}
                  onChange={(newValue) => setStartDt(newValue)}
                />
                <DatePicker
                  label="End Date"
                  value={endDt}
                  onChange={(newValue) => setEndDt(newValue)}
                />
                <TextField
                  label="Outlined secondary"
                  color="secondary"
                  value={'สรุปข้อมูลลูกหนี้ ผู้ป่วยใน '}
                  focused
                />
              </Stack>
            </LocalizationProvider>

            <CardActions>
              <Box flexGrow={1} />
              <Stack mt={4}>
                <Button onClick={onSubmit} color="primary" variant="contained">
                  Submit
                </Button>
              </Stack>
            </CardActions>

            {/* <Stack direction={'row'} gap={2} mt={2}>
              <Typography>{`last update : ${lastestOpd} `}</Typography>
            </Stack> */}
          </CardContent>
        </Card>
      </Stack>

      <Divider sx={{ marginY: '30px' }} />

      <Box sx={{ width: '100%', height: '500px', typography: 'body1' }}>
        <TabContext value={valueTab}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <TabList
              onChange={handleChangeTab}
              aria-label="lab API tabs example"
            >
              <Tab label="สรุปข้อมูลลูกหนี้ ผู้ป่วยใน" value="1" />
            </TabList>
          </Box>
          <TabPanel value="1">
            <Box>
              <Stack direction={'row'} gap={2} marginTop={0}>
                <Typography
                  fontStyle={'bold'}
                  fontSize={'1.1rem'}
                  color={'#2e4ad8'}
                  mb={1}
                >
                  export Exel
                </Typography>
                <IconButton
                  aria-label="export"
                  color="primary"
                  onClick={exportCaseToExcel}
                >
                  <SystemUpdateAlt />
                </IconButton>
              </Stack>
            </Box>
            <Box style={{ height: 500, width: '100%' }}>
              <DataGrid
                rows={dataCaseOp}
                columns={columns}
                getRowId={(row) => row.acc_code}
                slots={{
                  toolbar: CustomToolbar,
                }}
              />
            </Box>
          </TabPanel>
        </TabContext>
      </Box>
    </>
  )
}
