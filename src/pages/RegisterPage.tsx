import * as React from 'react'
import Avatar from '@mui/material/Avatar'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import FormControlLabel from '@mui/material/FormControlLabel'
import Checkbox from '@mui/material/Checkbox'
import Link from '@mui/material/Link'
import Paper from '@mui/material/Paper'
import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import Typography from '@mui/material/Typography'
import CardHeader1 from '../../src/assets/main_background.avif'
import { Card, CardMedia } from '@mui/material'
import axios from 'axios'
import { LoginResponse } from '../app_type/login.type'
import { useDispatch, useSelector } from 'react-redux'
import { RootState, store, useAppDispatch } from '../store/store'
import {
  fetchLogin,
  loginSelector,
  loginStatus,
} from '../store/slices/loginSlice'
import { Try } from '@mui/icons-material'
import toast, { Toaster } from 'react-hot-toast'
import { useNavigate } from 'react-router-dom'

function Copyright(props: any) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {'Copyright © '}
      <Link color="inherit" href="http://warin.go.th">
        Warinchamrab Hospital
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  )
}

const apiUrl = import.meta.env.VITE_API_BACKEND_URL
// TODO remove,import { Navigate } from 'react-router-dom';
const registerUrl = `${apiUrl}/api/v2/authen/register`

export default function RegisterPage() {
  // const user = useSelector((state: RootState) => state.loginReducer.user)

  const [user,setUser] = React.useState<string | null>('')
  const [password,setPassword] = React.useState<string | null>('')
  const dispatch = useAppDispatch()
  const loginReducer = useSelector(loginSelector)
  const isSuccess = loginReducer.success
 
  const navigate = useNavigate()

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    const data = new FormData(event.currentTarget)
    type LoginInput = {
      username: FormDataEntryValue | null
      password: FormDataEntryValue | null
    }
    const loginBody: LoginInput = {
      username: data.get('email'),
      password: data.get('password'),
    }



    try {
      const result = axios.post(registerUrl , loginBody)
      console.log(result)
      navigate('/')
      toast.success(
        (t) => (
          <>
            <span>
              <Box>Successfully Registration:</Box>
              <Button
                onClick={() => toast.dismiss(t.id)}
                sx={{ marginLeft: '3px', marginTop: '3px' }}
              >
                Dismiss
              </Button>
            </span>
          </>
        ),
        {
          duration: 4000,
          style: {
            border: '1px solid #713200',
            padding: '16px',
            color: '#713200',
          },
          iconTheme: {
            primary: '#0ce445',
            secondary: '#FFFAEE',
          },
        }
      )
      return result
     
     
    } catch (error) {
      console.log(error)
      toast.error('error')
    }

   
  }

  return (
    <>
      <Toaster position="top-right" />
      <Grid
        container
        component="main"
        sx={{ height: '70vh' }}
        alignItems={'center'}
        justifyContent={'center'}
      >
        <Grid
          item
          xs={12}
          sm={8}
          md={7}
          component={Paper}
          elevation={6}
          square
          display={'flex'}
        >
          <Grid
            sx={{
              my: 8,
              mx: 2,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <CardMedia
              component={'img'}
              sx={{ width: '100%' }}
              image={CardHeader1}
              title="green iguana"
            />
            <Typography variant="h3" marginTop={2}>
              Hospital Infromation Claim Management
            </Typography>
            <Typography>{user}</Typography>
          </Grid>

          {!isSuccess && (
            <Grid
              sx={{
                my: 8,
                mx: 2,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Register
              </Typography>
              <Box
                component="form"
                noValidate
                onSubmit={handleSubmit}
                sx={{ mt: 1 }}
              >
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  autoFocus
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                />
                
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}
                >
                  Sign Up
                </Button>
                <Grid container>
                  
                  <Grid item>
                    <Button onClick={()=>navigate("/")}>
                      Back to Sign In
                    </Button>
                  </Grid>
                </Grid>
                <Copyright sx={{ mt: 5 }} />
              </Box>
            </Grid>
          )}
        </Grid>
      </Grid>
    </>
  )
}
