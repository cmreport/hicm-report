export interface LoginResponse {
  result: string
  token: string
  message: string
}

export interface LoginErrorResponse {
  result: string
  message: string
}
