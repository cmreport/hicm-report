import { resolve } from 'path'
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'

// https://vitejs.dev/config/
export default defineConfig({
  // base: './',
  // server: {
  //   host: true,
  //   port: 3000
  // },
  plugins: [react()],
})
