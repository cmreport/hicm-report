FROM node:20-alpine AS builder

LABEL maintainer="Satit Rianpit <rianpit@gmail.com>"

RUN mkdir -p /app/node_modules && chown -R node:node /app

WORKDIR /app

RUN apk add --no-cache python3 g++

COPY . .

RUN npm i && npm run build

RUN apk add --no-cache g++

RUN npm i -g pm2

CMD ["pm2-runtime", "--json", "/app/process.json"]